---
title: Resources
groups:
  - items:
      - item: Women's Cultural Organizations & the NEA in the early 1980s A
      - item: Women's Cultural Organizations & the NEA in the early 1980s B
      - item: Snapshot of Foundation Support for Feminist Gender Justice Media
      - item: Reports on Gender Bias in Media – Mostly During 2012
      - item: Media to Build Reproductive Justice
      - item: 'Feminist Film and Video Conference, 1975: NY and LA'
      - item: Ariel Dougherty email Reponse
tableFields:
  - {}
---

