---
title: 'Feminist Film and Video Conference, 1975: NY and LA'
itemType: gallery
poster: /archive/media/conf-75.jpg
collections:
  - {}
resources:
  - {}
  - file: /archive/media/cffvo-packet-plus-2018-copy.pdf
keywords:
  - {}
related:
  - items:
      - {}
images:
  - {}
  - image: /archive/media/conf-75.jpg
---
Preceedings for Conference of feminist film and video organizations, February 1 and 2, 1975. Contributed as a PDF by Ariel Dougherty.
