---
title: Reports on Gender Bias in Media – Mostly During 2012
itemType: gallery
poster: /archive/media/33-reports.jpg
collections:
  - {}
resources:
  - {}
  - file: >-
      /archive/media/33-reports-on-media-gender-bias-in-2012-addendum-to-eeoc_.pdf
keywords:
  - {}
related:
  - items:
      - {}
images:
  - {}
---
Plethora of Reports on Gender Bias in Media – Mostly During 2012

– compiled by Ariel Dougherty Media Equity Collaborative @mediaequity

The number – 25 – of reports that came out during the first six months of 2012 prompted collection of this data

into a table. The sheer number, range of different forms of media, and the persistence lack of change speak to deeply

entrenched cultural attitudes of bias against women in all aspects of the media & culture. Here are listed 47 separate

reports that collectively map out extensive gender bias in all aspects of media.
