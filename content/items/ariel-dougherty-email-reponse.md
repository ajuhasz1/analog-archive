---
title: Ariel Dougherty email Reponse
itemType: gallery
poster: /archive/media/ad-email-pic.jpg
collections:
  - {}
resources:
  - {}
  - file: /archive/media/ariel-dougherty-email-response.pdf
    title: Ariel Dougherty email Reponse
keywords:
  - {}
related:
  - items:
      - {}
images:
  - {}
---
Email response to the annotated Research Meeting from Ariel Dougherty, one of the co-founders of Women Make Movies
