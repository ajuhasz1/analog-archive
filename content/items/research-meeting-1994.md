---
title: >-
  "Women of Vision" Research Meeting on Feminist Film and Video History,      
  NYC 1994
itemType: video
mediaUrl: >-
  https://archive.org/download/WomenOfVisionNewYorkMeeting_201810/Women%20Of%20Vision%20-%20New%20York%20Meeting.mp4
poster: /archive/media/firefoxscreensnapz002.jpg
collections: []
resources:
  - file: /archive/media/log_ny_tape_1.pdf
    title: '"Log NY Tape 1" (short, by Alex Juhasz)'
  - file: /archive/media/ny_tape_1.pdf
    title: '"Log NY" (2 pages with yellow, by Alex Juhasz)'
  - file: /archive/media/misc..pdf
    title: '"Log NY" (long, handwritten, no timecode, by unknown transcriber)'
  - file: /archive/media/log_ny_tape_1.pdf
    title: '"NY Log" (1/2, cursef, with TC, by unknown transcriber)'
  - file: /archive/media/log_nyc_tape_2.pdf
    title: '"NY Log" (2/2, cursef, with TC, by unknown transcriber)'
  - file: /archive/media/minutes.pdf
    title: '"NY Log" (in "minutes" and "big ideas," by unknown transcriber)'
  - file: /archive/media/ny_conference.pdf
    title: '"NY log" (big ideas, incomplete, by unknown transcriber)'
keywords:
  - value: Feminism
  - value: Women of Vision
  - value: film
  - value: video
  - value: history
  - value: media research
  - value: affect
  - value: media archive
  - value: documentary footage
  - value: VHS
  - {}
date: '1994'
related:
  - items:
      - item: 'https://wfpp.cdrs.columbia.edu/pioneer/ccp-alice-guy-blache'
    title: >-
      Alice Guy Blache (from the Women's Film Pioneer Project, Columbia
      Unviversity).
annotations:
  - body: >-
      <span class="color blue">Alex Juhasz</span> will introduce the project and
      the goals of the meeting over the next ten minutes. The twenty minutes
      after that (00:00:10-00:00:35) are comprised of brief introductions,
      around the circle, to the women who are in the room (many more will enter
      over the course of the day). <span class="color red"> The camera is
      floating about again, and so I see my friends <span class="color
      blue">Erin Cramer</span> and <span class="color blue">Alisa Lebow</span>
      (they both worked on the project). I see <span class="color blue">Amber
      Hollibaugh</span>. I introduce myself and explain what I work on. I see
      <span class="color blue">Su Friedrich</span> and <span class="color
      blue">Liz Kotz</span>. What happened to her? There’s <span class="color
      blue">Sarah Schulman.</span> I worry that I'm going on for too long.
      </span><span class="color blue"> Alex Juhasz</span> says, "I'm teaching at
      Bryn Mawr." <span class="color red"> Phew, now I know what year it is
      (1994), and where I am in my own life/work cycle! Living in Philly with
      <span class="color blue">Cheryl (Dunye)</span>.</span>
    end: '0:03:30'
    start: '0:01:31'
  - body: >-
      <span class="color blue">Alex Juhasz</span>: Explains that for this
      research project there is some tension around the "academization of
      feminist film" and that  "I embody the academic side." <span class="color
      red"> Then: I register ambivalent certainty about my "side." Now I just
      embody that side. And others. Then, I posed and willed myself into being
      for the women in the room as well as for the camera (and the future). I
      pretended to embody an "academic." And in this way, I embodied her. To be
      taped in the doing. To be known again, now, as I was and as I
      became.</span>
    end: '0:05:01'
    start: '0:04:04'
  - body: >-
      <span class="color red">There's <span class="color blue">Annie
      Sprinkle</span> and <span class="color blue">Terry Lawler.</span> And
      <span class="color blue">Abigail Child</span>, I think. Wow, I like
      Alisa's short hair …  and soft face. Are people actually listening to me?
      It looks like maybe not. I'm pretending to embody competence but most of
      them are  older than me, and certainly more important and even famous (at
      least to me). They are also really uncertain of what I am going to do
      (with them).</span>
    end: '0:06:19'
    start: '0:05:02'
  - body: >-
      <span class="color blue">Alex Juhasz</span>: “I could sit in a library
      and  get this 'history' faster, but I'm interested in collective work. I
      want multiple voices. Especially about women's politics. This is my
      vision, that many will contribute to the ideas." <span class="color red">
      This continues to be my vision 25 years after the fact, as well as my
      process in this revisit to the project, what would become "Women of
      Vision." By putting the VHS research footage online into this tool I allow
      many others to use it, and add to it, and continue to "contribute to the
      ideas." </span>
    end: '0:07:04'
    start: '0:06:20'
  - body: >-
      <span class="color blue">Alex Juhasz</span>: "I am not sure what is next
      for the project, but it will develop." <span class="color red"> I still do
      research this way, which is no surprise I guess, since I am still myself!
      Opening into possibility via collaboration. Open to process while
      dictating direction. And it's kind of cool to see that the shape of the
      project has developed into this: using an online format that many in the
      tape conjectured would be a helpful and appropriate format for feminist
      history making (see the last hour, i.e. 2:03:40, and thoughts about
      technology from <span class="color blue">Shu Lea Cheang, Shari Frilot,
      Cylena Simonds, </span> as just one example of our interest, then, in a
      just-developing digital horizen).
    end: '0:08:05'
    images: []
    start: '0:07:05'
  - body: >-
      <span class="color blue">Alex Juhasz</span> shares the agenda for the
      meeting.
    end: '0:09:32'
    images: []
    start: '0:09:11'
  - body: >-
      <span class="color blue">Alex Juhasz</span> gives thanks to her
      collaborators in the room: <span class="color blue">Alisa Lebow, Erin
      Cramer, Truc Ha, Lisa Rodriguez-Rodriguez,</span> and <span class="color
      blue">Megan Cunningham.</span>
    end: '0:09:55'
    start: '0:09:33'
  - body: >-
      <span class="color blue">Terry Lawler</span>, on behalf of the meeting’s
      host, Women Make Movies, greets the group and explains what they do.
    end: '0:14:30'
    start: '0:10:00'
  - body: >-
      <span class="color blue">Amber Hollibaugh</span>, director Lesbian AIDS
      Project and activist videomaker.
    end: '0:15:45'
    start: '0:14:30'
  - body: >-
      <span class="color blue">Alisa Lebow</span>: film scholar, AIDS activist
      video maker, and co-producer of Women of Vision.
    end: '0:16:20'
    start: '0:15:45'
  - body: >-
      <span class="color blue">Erin Cramer</span>: screenwriter and director,
      V-Girl, and co-producer of Women of Vision. <span class="color red"> Heard
      but not seen. Sitting behind a pole. This venture had none of the
      now-requisite professionalism that would come to matter for most
      independent media and thus feminist media (listen to the <a
      href="https://soundcloud.com/angela-aguayo/megan-alex/s-3WWsm"> podcast
      interview</a> that I made for the <a href="
      https://fmh.ucpress.edu/content/5/4"> Feminist Media History</a> revisit,
      with Megan Cunningham, who was behind the camera, an undergraduate, and
      shooting this meeting. She became the founder and CEO of a multi-million
      dollar media company, Magnet Media, "a production company and an
      interactive marketing services firm." We get a quick look at her, and an
      introduction, at 00:31:00.)</span>
    end: '0:16:50'
    start: '0:16:20'
  - body: >-
      <span class="color blue">Juanita Mohammed</span>: AIDS activist and
      community-based video maker. <span class="color blue">Alex Juhasz's</span>
      collaborator, with others, on "We Care: A Video for Care Providers of
      People Affected by AIDS" (1990). Mohammed is one of the featured
      interviewees in the "Women of Vision" book and documentary and also a
      contributor to the <a href=" https://fmh.ucpress.edu/content/5/4">
      Feminist Media Histories</a> project re-visit, for which she made a <a
      href="https://vimeo.com/340243804/230a094b5b"> video conversation</a>
      about feminism with her granddaughter, Pharah Diaz, connecting their poems
      about power, aging, beauty and violence against women. <span class="color
      red"> Juanita's work has always enjoyed a community-based aesthetic. She
      will speak powerfully, across this session (see 01:52:00, one of many
      debates this day about the many affordances of video), about what video
      means and does for regular people, especially poor women of color.</span>
    end: ' 0:17:00'
    images:
      - image: /archive/media/we-care.jpg
      - image: /archive/media/strong.jpg
    start: '0:16:50'
  - body: >-
      <span class="color blue">Annie Sprinkle</span>: feminist porn, performance
      and video artist.
    end: '0:18:30'
    start: '0:17:15'
  - body: >-
      <span class="color blue">Mali Wu</span>: Filmmaker from Taipei. <span
      class="color red"> She's speaking so softly that most of her contribution
      is lost. Earlier in the meeting (14:30, although this detail, like most,
      is not annotated), I had asked people (women) to speak up, even if
      uncomfortable for them, because we had learned from our previous recording
      (in Philaldelphia) that soft-spoken participants would be lost to history,
      or at least its video record.</span> <span class="color
      blue">Erica,</span> a student from Columbia, explains that she is at the
      meeting to babysit, but there's no kids. A conversation about children,
      childlessness, and feminist artists and filmmakers ensues where <span
      class="color blue">Alex Juhasz</span> reports some of what she has heard
      about kids at the previous meeting. <span class="color red"> This is too
      rich, given that <span class="color blue">Cheryl Dunye</span> will speak
      next and the camera pans to her. In 1998 and 1999 we will have two
      children, not yet part of the conversation but perhaps already in our
      shared imaginations. </span>
    end: '0:20:20'
    start: '0:18:30'
  - body: >-
      <span class="color blue">Cheryl Dunye</span>. Experimental and narrative
      filmmaker. <span class="color red">My partner at the time.</span> Cheryl
      says she’s "Working on my first feature." <span class="color red">She's
      gorgeous. Can’t help but think about the feature (and future: two kids!)
      that gets made: "The Watermelon Woman" (1996), <span class="color
      blue">Simone Dunye</span> (1998), <span class="color blue">Gabriel
      Juhasz</span> (1999). Could get stuck in the feelings--loss, love,
      memory--needing to stop the tape's forward flow.</span>
    end: '0:21:20'
    images:
      - image: /archive/media/wmw.jpg
    start: '0:20:20'
  - body: >-
      <span class="color blue">Lisa Rodriguez-Rodriguez</span>. Project
      assistant on "Women of Vision."
    end: '0:21:40'
    start: '0:21:20'
  - body: >-
      <span class="color blue">Maria Bautis</span>: Latin American feminist
      filmmaker. She says she’s making a video on Latinas and AIDS and is
      desperate to get money. <span class="color red">Just as I was for this,
      and every feminist activist media project! Like we all were and are
      (except for some of us, who have begun to work in the mainstream, like
      <span class="color blue">Cheryl</span> for instance, albeit not until her
      50s and with all thanks to <span class="color blue">Ava DuVernay</span>
      and her stated commitment to only hiring women directors.) What would have
      happened if more of us got it, then and now, that funding we really needed
      or thought we needed to get something done (right)? </span>
    end: '0:25:00'
    images:
      - image: /archive/media/ava.jpg
    start: '0:22:00'
  - body: >-
      <span class="color blue">Liz Kotz</span>. Scholar of feminist art and
      film. <span class="color red">Now I remember seeing her a few years back,
      maybe at a screening in L.A. It seems, I am experiencing this tape as
      something most akin to a home movie of feminist media worlds: a moving
      marker of who was there with me and of me being there. Of who I know and
      have known. Of what I did. Of how we've changed. Of what I’ve lost.</span>
    end: '0:27:00'
    start: '0:26:00'
  - body: >-
      <span class="color blue">Su Friedrich</span>. Experimental lesbian
      filmmaker
    end: '0:29:00'
    start: '0:28:00'
  - body: >-
      <span class="color blue">Ella Shohat</span>. Film scholar of post-colonial
      and Arab cinema.
    end: '0:30:00'
    start: '0:29:30'
  - body: >-
      <span class="color blue">Sarah Schulman</span>. Lesbian novelist,
      screenwriter, and film curator.
    end: '0:30:20'
    start: '0:30:00'
  - body: >-
      <span class="color blue">Beth Stryker</span>. Filmmaker and arts
      administrator.
    end: '0:30:40'
    start: '0:30:20'
  - body: >-
      <span class="color blue">Megan Cunningham</span>, co-producer, Women of
      Vision, and undergraduate student from Swarthmore College. <span
      class="color red"> Her camera films her feet, and cords, and then finally
      her face. Megan was one of 21 women who were interviewed for the final
      "Women of Vision" project (as part of its commitment to a reflexive
      production process) and also engages with me for the <a href_
      https://fmh.ucpress.edu/content/5/4"> Feminist Media Histories</a> special
      issue, via a <a
      href="https://soundcloud.com/angela-aguayo/megan-alex/s-3WWsm"> podcast
      conversation</a> about her current work as CEO of Magnet Media.</span>
    end: '0:32:00'
    start: '0:31:00'
  - body: >-
      Timeline debate ensues. Does our timeline begin when women are in front of
      our behind the camera? Enter <span class="color blue">Cylena
      Simonds</span>: film programmer and scholar. “Is this progressive?
      Including porn and objectification?”
    end: '0:39:00'
    start: '0:37:00'
  - body: >-
      Timeline debate continues and now we are considering what counts as
      “feminist.” <span class="color blue">Cheryl Dunye</span> mentions <span
      class="color blue">Spike Lee</span> and <span class="color blue">Leslie
      Harris</span>. <span class="color red">I just saw Leslie Harris at the BAM
      Black 90s Cinema Retrospective presenting her debut “Just Another Girl on
      the IRT” (1992). Like most black women filmmakers, she has yet to make her
      second feature, although she’s working on it.</span> <span class="color
      blue">Yvonne Rainer</span> enters the conversation, experimental feminist
      lesbian filmmaker and choreographer and my former instructor from the
      Whitney Independent Studio Program.
    end: '0:46:00'
    images:
      - image: /archive/media/just-another.jpg
    start: '0:43:20'
  - body: >-
      <span class="color blue">Alex Juhasz</span>: maybe this should be a
      history of work that never got done. <span class="color red">Although this
      history did get done and is getting done again. How do feminist things get
      done? What isn't made? Why did this? A matter of my willpower, or
      professional position, my process or connections? The (small but real)
      value of the project to myself and others? And of course, getting done
      doesn't mean getting seen ... or lasting. </span>
    end: '0:55:30'
    start: '0:55:00'
  - body: >-
      <span class="color blue">Terry Lawler</span>: Feminist film needs to be
      coupled with a political agenda, so it can’t really start until the 1970s.
      But can’t we draw connections to other movements: civil rights,
      documentary, avant-garde, experimental? <span class="color blue">Abigail
      Child</span>: we knew our politics. I was there. <span class="color
      blue">Liz Kotz</span> adds more names: <span class="color blue">Carolee
      Schneemann</span> and <span class="color blue">Barbara Rubin</span>. What
      about experimental filmmakers? <span class="color blue">Patty
      White</span>: what about docs? avant-garde? <span class="color blue">Gita
      Reddy</span>: and what about women of color?
    end: '01:01:00'
    images:
      - image: /archive/media/cls.jpg
      - image: /archive/media/barbara-r.jpg
    start: '0:56:00'
  - body: >-
      <span class="color blue">Abigail Child</span>: “I was there. We were
      hungry. It didn't come out of the mud. It was collective. We were hungry.”
      <span class="color blue">Liz Kotz</span>: It didn’t just happen at school.
      People who weren’t academics participated but had other senses of history.
      This history is not just a “Women and Film” syllabus. You went to movie
      screenings. People saw work, but not only at school. <span class="color
      red"> I wonder what Liz would think about people watching tapes, like this
      one, online. Not at school. Can't help but think, again, of another of my
      process-heavy, digial, feminist video research projects <a
      href="http://vectors.usc.edu/projects/learningfromyoutube/"> "Learning
      from YouTube</a>."</scan>
    end: '01:03:00'
    start: '01:01:00'
  - body: >-
      <span class="color blue">Alex Juhasz</span>: Perhaps this is a history of
      access and distribution, or movements and not movements. Of other formats:
      performance for instance, where women entered earlier. A history written
      about what is and isn't seen. <span class="color red">For me now, it is
      painful, eerie, telling, and troubling that I and we get stuck on the same
      problems, practices, and potential solutions like this question of what is
      seen and made and what is not. Where have we found traction and moved the
      baseline forward: the number of productions and successful feminist
      artists? the diminishing barriers to access? And what stays the same or
      retreats?</span><span class="color blue"> Cheryl Dunye</span> suggests:
      maybe follow one film like "Daughters of the Dust" (<span class="color
      blue">Julie Dash</span>, 1991)? <span class="color red"><span class="color
      blue">Julie Dash</span>, like <span class="color blue">Leslie
      Harris</span>, had a hard time making her second feature, but has enjoyed
      a real career boost since directing "Queen Sugar." There is a solution.
      </span><span class="color blue">Cylena Simonds</span>: what about
      performance?
    end: '01:07:30'
    images:
      - image: /archive/media/d-of-d.jpg
      - image: /archive/media/julie-d.jpg
    start: '01:05:07'
  - body: >-
      <span class="color blue">Sarah Schulman</span>: It’s hard to reach hetero
      women. <span class="color blue">Maria Bautis</span>: speaks about the
      importance of oral histories of Latinas. <span class="color blue">Su
      Freidrich</span> and <span class="color blue">Liz Kotz</span> on the
      dearth of women’s film festivals in the 1990s.
    end: '01:17:00'
    start: '01:12:23'
  - body: >-
      <span class="color blue">Alex Juhasz</span> on how mainstream media
      parallels independent media and activist media. <span class="color
      blue">Gita Reddy</span> on avoiding the “lure of origins.”
    end: '01:29:00'
    start: '01:25:00'
  - body: >-
      <span class="color blue">Erin Cramer</span> speaks about competing and
      multiple histories within this history and how that can be expressed with
      different technologies and for different audiences. <span class="color
      blue">Alex Juhasz</span> suggests “we could do it through a computer. This
      is a linear meeting. How do you mark a matrix? How to talk about history
      in more complex way?” <span class="color red"> We're "doing it through a
      computer" now! How do you like it?</span>
    end: '01:32:00'
    start: '01:30:00'
  - body: >-
      <span class="color blue">Su Freidrich</span> asks what has been written
      about women's film history? There is not a lot in the book stores. Not
      much about indie film. Let's ignore Hollywood! <span class="color
      blue">Terry Lawler</span> asks “why women don't write about feminist
      film.” <span class="color blue">E. Ann Kaplan</span> says “It’s hard to
      find feminist films.” <span class="color blue">Abigail Childs</span> and
      <span class="color blue">Liz Kotz</span> counter: this is exactly what
      feminist scholars should be doing! <span class="color red">We do know
      these histories now. They have been researched, written about, and taught,
      in "Feminist Media Histories" and other feminist media journals and
      projects that we founded and built from nothing, still with less funding
      than we need but accomplished even so. Take the <a
      href="https://wfpp.cdrs.columbia.edu/"> Women Film Pioneers Project</a> or
      Su Friedrich's recent <a
      href="https://arts.princeton.edu/events/women-film-editors/"> Edited By:
      Women Film Editors</a> (one supported by Columbia the other Princeton).
      This has happened at a great pace and volume since this meeting and
      because of us. We realized many of the hopes for the future expressed here
      by doing them.</span>
    end: '01:40:30'
    images:
      - image: /archive/media/e-ann-book.jpg
      - image: /archive/media/new-fuck.jpg
    start: '01:32:00'
  - body: >-
      <span class="color blue">Alex Juhasz</span> asks: What do we want and need
      from history? <span class="color blue">Su Freidrich</span> wants a book.
      <span class="color blue">Liz Kotz</span> explains this is how people enter
      history; and have power, according to <span class="color blue">Abigail
      Child</span>. <span class="color red"> One of the more painful parts of
      all this--return, rethinking, re-use, re-watch--is that of course I did
      write a book ... and it mattered and it didn't. It was used and it wasn't.
      I imagine the lesson is about volume, and scale (what we all do at about
      the same time, and building steadily and connectedly in time), and not
      about the use or value of any one thing (whether it itself or you yourself
      did it right or well), even as each of us works so very hard on our own
      one thing ... Until there is a lot of stuff, enough attention, it is hard
      to register and take in the weight and complexity of a movement built of
      all those precious components. When and if scale comes--by way of
      mainstream attention or "popular feminism" or a legion of
      compatriots--much of the nuance, complexity, and radicality that is
      evidenced in the hearts of movements, like this meeting (in its very
      duration and diversity) is likely to be lost. So much to take in and pass
      on, our output is sped through or simplified by annotation and other
      methods of return.</span>
    end: '01:43:00'
    start: '01:40:00'
  - body: >-
      <span class="color blue">Annie Sprinkle</span> insists: “This history is
      complex, feminism is multiple, it needs a form the can honor that: polka
      dot or plaid, not linear.” <span class="color red"> Someone off screen,
      <span class="color blue">Patty White?</span> jokes: "Alex is not going to
      make a self-reflexive film about how hard it is to make a film about
      feminist film history." <span class="color red"> No. I didn't. I saved
      that for here it seems. <span class="color blue">Maria Bautis</span> and
      <span class="color blue">Amber Hollibaugh</span> make suggestions about
      useful forms for telling this history: don’t go linear, create
      intersections not parallels. <span class="color red"> Perhaps this is
      simply a matter of tech? Look where we've come. Non-linear editing
      software and internet infrastructures for self-authoring and polka dots
      and plaids. </span>
    end: '01:49:00'
    start: '01:44:12'
  - body: >-
      <span class="color blue">Yvonne Rainer</span> talks about a 1975 interview
      with her for the first Camera Obscura with <span class="color blue">Jackie
      Reynal</span> and <span class="color blue">Lina Wertmüller</span>.
    end: '01:51:00'
    images:
      - image: /archive/media/jackie-r.jpg
      - image: /archive/media/lina_wertmuller-2-870x1024.jpg
    start: '01:49:00'
  - body: >-
      A discussion about the value of video in this history. Another
      unidentified woman and <span class="color blue">Martha Gever</span>
      feminist media scholar and writer, enter the conversation. Gever presses
      that it is important to "keep video in the frame." <span class="color
      blue">Abigail Child</span> disagrees. As an experimental, feminist
      filmmaker she was working with video makers. Dividing out film and video
      is troublesome. Sure there were video artists like <span class="color
      blue">Joan Jonas</span> and <span class="color blue">Lynda Benglis</span>,
      but Child comes out of collective activist movements in the early 70s
      "zooming on a political agenda." <span class="color blue">Juanita
      Mohammed</span> suggests video allows for access and <span class="color
      blue">Cheryl Dunye</span> suggests that video is one of many possible
      “nodes” that might be useful to draw out the intersections that are lived
      across this history.
    end: '02:01:45'
    images:
      - image: /archive/media/joan-j.jpg
      - image: /archive/media/lynda-b.jpg
    start: '01:52:00'
  - body: >-
      <span class="color blue">E. Ann Kaplan</span> and <span class="color
      blue">Liz Kotz</span> add more nodes: Paper Tiger, artists. <span
      class="color blue">Shu Lea Cheang</span>—experimental media artist—enters
      the conversation, raising the importance of naming technologies over
      calling out names. <span class="color blue">Alex Juhasz</span> adds
      camcorders as a node. <span class="color blue">Shu Lea Cheang</span> adds
      the CD Rom. <span class="color blue">Cylena Simonds</span> asks:  “Have
      you heard about Mosaic? It can be put on the internet. Exciting! Maybe
      video isn't the best medium.” <span class="color red">OMG. Technology is
      what is truly different since then! Along with the access that some of
      these feminists have gained to larger audiences or budgets largely through
      the support of other women as related to the current popularization of
      feminism as related to internet. A circle of possibility, perhaps?</span>
      <span class="color blue">Cheryl Dunye</span> agrees, as does <span
      class="color blue">Beth Stryker</span>. <span class="color red"> On yet
      another review of this footage, proofing and checking these annotations, I
      spy <span class="color blue">Kathy High,</span> video artist and writer,
      behind Shu Lea. I take a look at my attendance list (which has people's
      addresses and phone numbers on it, hence I can't include it here). Also in
      attendance, but not seen by me on this revisit: <span class="color
      blue">Jane Castle, Michelle D'Acosta, Sherry Millner, Elisabeth Lyon,
      Martha Rossler, Rose Troche</span>. Meanwhile, my "New York No Show" list
      (people who rsvp'ed but didn't end up making it?) has 50 more power houses
      I/we never got/get to hear from, from <span class="color blue">Peggy
      Awesh</span> to <span class="color blue">bell hooks</span> and <span
      class="color blue">Camille Billops</span> (and that's just three  of the
      As and Bs!)
    end: '02:09:00'
    images:
      - image: /archive/media/peggy-a.jpg
      - image: /archive/media/bell-h.jpg
      - image: /archive/media/camille-b.jpg
    start: '02:03:40'
  - body: >-
      <span class="color blue">Sarah Schulman</span> and <span class="color
      blue">Terry Lawler</span> discuss the size of the audience for this
      project and other feminist projects: small or big, personal or
      institutional.
    end: '02:11:00'
    start: '02:09:00'
  - body: >-
      <span class="color blue">Amber Hollibaugh</span>: How ‘bout you make
      videotape for a complicated understanding? You can take your time. And
      someone else can do it later. Someone else can come along! The recording
      of history and making of history allows someone else to have the footage
      you shot. If every woman in this room did a three hour interview for you
      that would be a great resource for the future. If we did this, there would
      immediately be a history that no one could take away from us. <span
      class="color red">OH MY. I'm (still) doing it. That is, I am now looking
      at what I shot then, as she said I should and would. Or rather: someone
      else would (but hasn't, yet). I did interview twenty people. I put those
      tapes in a documentary and then also an archive for others to visit. But
      today much of that impulse is at least partially satisfied by YouTube,
      with way more stuff than any one project could render. Given the internet,
      does anyone really need these four more hours here? Do I, in any way other
      than as my kind of personal, communal, institutional home movie? Is there
      something in its feminist academic media activist process and content that
      adds value outside of what is now made available in large and diverse
      scale online? How do we idenitfy the distinct contributions of scholars
      and historians: focus, depth, method, style, comprehensiveness, language?
      Co-editor of the special issue, Angela Aguayo, and I invited authors for
      our special issue to work with inventive and artistic methods around
      inter-generational conversation (like this one), because we are keen on
      and committed to expanded methods for feminist activist analysis and
      history.</span>
    end: '02:13:00'
    start: '02:11:00'
  - body: >-
      <span class="color blue">Alex Juhasz</span> explains what was discussed at
      the first, Philadelphia meeting, including a conversation about what
      "feminism" means. In Philadelphia woman also spoke about this history as
      being one of obstacles, lack of access, films never being made, and not
      being successful and nevertheless making the work. Unlike the women in
      this meeting in New York, the vast majority of them did not have a
      national or international reputation. People suggest what hasn’t been
      heard yet: <span class="color blue">Sarah Schulman</span>,
      “personalities.” <span class="color blue">Annie Sprinkle</span>: “things
      that aren’t obvious.” <span class="color blue">Amber Hollibaugh</span>:
      the fact that she was on the left and not primarily identified as a
      feminist, early on.
    end: '02:19:50'
    start: '02:13:00'
  - body: >-
      <span class="color blue">Shari Frilot</span> enters the conversation,
      black lesbian filmmaker and programmer. “It's all about access to
      resources. And venues to show work for women of color." She shares a story
      about how she began. "One woman shares a call for work and another feels
      invited to make work." <span class="color red">Amazing! she's doing just
      this at the grandest of scales now, at Sundance. It's so moving to see the
      women who willed themselves into the positions they dreamed and theorized
      as necessary. So many of these women, really. And then, so many of them
      didn't. See thoughts on Philly, above.</span><span class="color blue">
      Cheryl Dunye</span> agrees and discusses how feminist literature can call
      WOC to action. <span class="color blue">Erin Cramer</span> continues.
    end: '02:22:45'
    start: '02:19:50'
  - body: >-
      <span class="color blue">Yvonne Rainer</span> says maybe everything
      doesn't need to be put into a historical context. "The theorists have left
      the room." She asks, “How do you, Alex, fit in?”
    end: '02:24:00'
    start: '02:23:20'
  - body: >-
      A conversation ensues about how to make the documentary film about
      feminist media. <span class="color blue">Annie Sprinkle</span>: “interview
      lots of women and we can do many things with it. Gather this stuff. Make
      several products. Or archival things.” Or make it a piece about the need
      for history. <span class="color red">Like this, Annie? </span> Then
      conversation about form with <span class="color blue">Yvonne
      Rainer</span>.
    end: '02:32:00'
    start: '02:24:00'
  - body: >-
      A conversation about everything feminists, or students, or women don’t
      know about feminism including thoughts from <span class="color blue">Sarah
      Schulman</span>, <span class="color blue">Erin Cramer</span>, and <span
      class="color blue">Annie Sprinkle</span>. <span class="color blue">Kathy
      High</span> enters the conversation, feminist videomaker, writer and
      curator, asking how do you introduce processes and not just great works?
      <span class="color red">FYI: at this point, I'm annotating in large
      paragraphs about greater durations. This process is painstaking and
      trying, and given how long the tape is, and how tired I get by the time I
      arrive here, at 02:41:00, I have little confidence that anyone but me has
      gotten this far, this deep into the weeds. But then, in
      counter-distinction, every time I return to the tape and find and hear
      another amazing gem of vivid conversation from another brilliant
      colleague, I want to pluck it out and quote it and thereby highlight it
      and share it in hopes that others see and hear it, too. (Random process
      experiment here, as per the suggestion of <span class="color blue">Erin
      Cramer</span> who read and commented on these annotations for me. I just
      dropped the needle and heard <span class="color blue">Sarah
      Schulman</span> on the complex and troubled relationship between
      experimental form and political commiment \[2:34:18] and then <span
      class="color blue">Mona Jimenez</span> on video: "there were things that
      happened with video that were not about distribution. Not product oriented
      \[1:20:02] but rather documentation only useful for a short period of
      time." How do you find and figure that for this history?, she asks. And I
      deliver, in my way: with this video on the internet.)</span>
    end: '02:41:00'
    start: '02:32:35'
  - body: >-
      <span class="color blue">Amber Hollibaugh</span> asks: ““what do you want?
      What about the process of making it and the community that is built by
      making it! Think about AIDS video. The process of the making of it is what
      matters. What's it for and who's it for? How would you know you
      succeeded?” <span class="color blue"> Alex Juhasz</span> answers: “I have
      faith in the process.” <span class="color red"> I can’t believe I'm saying
      this. This is what I said earlier writing now about what I'm doing here.
      This really is too circular. Too self-referential. Too similar to itself
      and me and the community's values I try to inhabit and engender. Also, if
      I have to do it again, so soon, it must not have succeeded, right?</span>
      <span class="color blue"> Alex Juhasz</span>: “I was thinking of important
      events and films. There are more detailed histories I want to touch.
      Through the details of history.” <span class="color red"> Is that what
      this red typing is, just in a different way, "including the details of
      making history" anew and again, enabled by this format and today's
      technologies? Details of affects of history (making)? </span> <span
      class="color blue"> Sarah Schulman</span> continues: you need money or
      relations to succeed. <span class="color blue"> Cheryl Dunye</span>:
      You're making it for someone. <span class="color blue"> Yvonne
      Rainer</span>: it needs to be compelling. <span class="color red"> I
      suppose it wasn't (and isn't?) compelling enough. Maybe one needs the
      talent of a <span class="color blue"> Cheryl Dunye</span> or <span
      class="color blue"> Yvonne Rainer</span> or <span class="color blue">
      Sarah Schulman</span> for that. Some of the many women from this meeting
      who continued to gain attention, support, fame, and power in the years
      after this meeting as lesbian feminst artists. <span class="color red"> Do
      we want our history to be linked to talent? And what of process?
      Technology? Community? This is hard!</span>
    end: '02:45:40'
    start: '02:41:06'
  - body: >-
      <span class="color blue">Alex Juhasz</span> asks: “This is a woman’s
      history. How do you show things that aren't visible?” <span class="color
      blue">Cheryl Dunye</span>: “and those that are?" <span class="color
      blue">Erin Cramer</span> returns the conversation to video. A technology
      that allows woman's work to get made.
    end: '02:48:10'
    images:
      - image: /archive/media/third-world-majority.jpg
    start: '02:45:40'
  - body: >-
      The discussion turns to audiences and communities. <span class="color
      blue">Annie Sprinkle</span>: “There are different kinds of audiences. I
      was making porno for men. Then women, then the art world, then feminists.
      I changed work for my audience.” <span class="color blue">Cheryl
      Dunye</span> and <span class="color blue">Terrly Lawler</span> discuss.
    end: '02:50:47'
    start: '02:48:10'
  - body: >-
      <span class="color blue">Alex Juhasz</span>: ”this is about 1994. How
      things are breaking down and things are falling apart.” <span class="color
      red">This is about 2019.</span>
    end: '02:50:55'
    images:
      - image: /archive/media/blm.jpg
    start: '02:50:50'
  - body: >-
      A debate about what and who counts as “feminist” with <span class="color
      blue">Cheryl Dunye</span>, <span class="color blue">Terry Lawler</span>,
      <span class="color blue">Alisa Lebow</span>, <span class="color
      blue">Shari Frilot</span> (who adds <span class="color blue">Christine
      Martin's</span> name to the mix and list). <span class="color blue">Sarah
      Schulman</span>: who is allowed to tell the story with what repercussions?
      <span class="color blue">Amber Hollibaugh</span>: pissed off daughters
      writing about 2nd wave? Is this how we know the success of a movement: we
      birth new possibilities including our own critiques. <span class="color
      red">I'm thinking: hey, that's me, too. Angry or at least very busy
      daughter of a second wave feminist, <span class="color blue">Suzanne
      Juhasz</span>. Will I admit it?</span> <span class="color blue">Annie
      Sprinkle</span>: “same thing in porn. One woman's liberation is another
      woman's bondage.”
    end: '03:05:00'
    images:
      - {}
      - image: /archive/media/romance-of.jpg
      - image: /archive/media/desire-for.jpg
    start: '02:51:12'
  - body: >-
      A meandering <span class="color red">perhaps exhausted? </span>
      conversation about the shape, distribution, form, and subject of this
      project: <span class="color blue">Cheryl Dunye, Terry Lawler, Patty White,
      Alisa Lebow, Shari Frilot, Erin Cramer, Yvonne Rainer, Maria Bautis, She
      Lea Cheang.</span> <span class="color red"> It is at once mortifying,
      fulfilling, generous, and importantly self-reflexive that all these media
      feminists are helping me to think about how to tell this/their/our
      history. </span>
    end: '03:27:00'
    start: '03:05:00'
  - body: >-
      A conversation about new technologies--CD Roms, the internet--and the
      future with <span class="color blue">Shari Frilot, Annie Sprinkle</span>,
      and <span class="color blue">Shu Lea Cheang</span>.
    end: '03:30:00'
    start: '03:27:25'
  - body: >-
      Naming what is not "obvious" but also very critical: AIDS, support, making
      big things. <span class="color blue">Annie Sprinkle, Shari Frilot, Yvonne
      Rainer, Sarah Schulman.</span>
    end: '03:35:30'
    start: '03:30:00'
  - body: >-
      A final less-spirited but still important discussion about process,
      audience, funding, politics and why cycles of voice or voicelessness
      repeat for women in connection to their sense of their own agency or lack
      there of. <span class="color red">URGH (must I clarify this response? Urgh
      that we talk about this over and over. Urgh that I write about and embody
      this here).</span><span class="color blue"> Juanita Mohammed, Terry
      Lawler, Patty White, Shari Frilot.</span> How do women imagine that they
      can be artists? <span class="color blue">Alex Juhasz</span>: this history
      is always one of certain women being able to break through, even though we
      are told we aren't allowed to speak in this society. How do you explain
      how some women can get past those constraints so others can follow? As a
      woman of color, or poor woman, in 1994, or 25 years later <span
      class="color red">hello, from 2019</span>, some women don't feel like what
      they have to say matters. <span class="color blue">Shari Frilot</span>:
      "The woman I was discussing recorded something. But she didn't feel like
      it mattered to anyone else. In the first black lesbian film festival <span
      class="color red">(see <a
      href=https://www.dukeupress.edu/sisters-in-the-life">"Sisters in the Life:
      A History of Out African American Lesbian Media-Making</a>, eds. Yvonne
      Welbon and Alex Juhasz, 2018, for more on this history)</span>, many of
      the participants thought they were artists. But they couldn't understand
      how they would get to Hollywood or even why other people would think what
      they have to say is important. So they do it in their room. I know, I was
      there: I was taping over things that could have been in my library right
      now." <span class="color red"> Shari, you're in my archive now. </span>"I
      could have had a big old library." <span class="color blue">Annie
      Sprinkle</span>: I want the people who really made it to the top to be
      taught. <span class="color blue">Cheryl Dunye</span>: "Who is that? <span
      class="color blue">Rose Troche? Martha Cooldige</span>? Is it you; is it
      me?" <span class="color blue">Patty White</span>: There's an argument for
      self-reflexiveness in the process. <span class="color red">Hi Patty.
      </span>"How and why has the history been written as names and dates?"
      <span class="color red">You didn't do this in your books, did you, Patty?
      We have developed many approaches and venues to tell and share this
      complex, personal, communal history!</span> People debate if a list of the
      10 most obvious people or events is useful. <span class="color
      red">Everyone looks exhausted. The camera lazes about. I have crumpled.
      </span> <span class="color blue">Cheryl Dunye</span> says "show a 40
      minute clip of "Grapefruit" (1989) by <span class="color blue">Cecelia
      Dougherty</span>. End with that." <span class="color blue">Sarah
      Schulman</span>: or "Near the Big Chakra" (1971, <span class="color
      blue">Anne Severson</span>). <span class="color blue">Alex Juhasz</span>:
      "Thanks for staying and coming. I really appreciate all your help. Please
      take my food. Our food." Weak applause. <span class="color red">Camera is
      not focused. Things wind down. Hey, look ... Just our luck! The tape is
      not 4+ hours. Or it is, but 32 minutes of that are snow.</span>
    end: '03:47:14'
    images:
      - image: /archive/media/rose-t.jpg
      - image: /archive/media/martha-c.jpg
      - image: /archive/media/cecelia-d.jpg
      - image: /archive/media/anne-s.jpg
      - image: /archive/media/patty-w-book.jpg
      - image: /archive/media/sis-in-life.jpg
    start: '03:37:00'
  - body: >-
      The VHS recording begins with a quick but confusing glitch. <span
      class="color blue">Alex Juhasz </span> says: "ok so anyway," and the tape
      sort of freezes and then goes again to black. For a long time. Stay with
      the black. At last, <span class="color blue">Alex Juhasz </span> returns,
      rustling about in her bag. "So tired," she says. The camera aimlessly,
      sloppily pans right and then back to Alex again. And then: "No, I'm okay."
      Another glitch (camera turned on and off?) Then, the actual formal
      conversation begins. <span class="color red"> Then: my hair is black. My
      face is unlined and soft. <span class="color blue">Amber </span>sits
      beside me, younger as well. I have on a leopard head-band!</span>
    end: '00:10:01'
    start: '00:00:01'
  - body: >-
      <span class="color blue">Alex Juhasz</span>, explains why they are taping
      the meeting.
    end: '14:30'
    start: '00:14:00'
  - body: >-
      <span class="color blue">Gita Reddy</span>. Film scholar, programmer, and
      actor.
    end: '0:22:00'
    start: '0:21:40'
  - body: >-
      <span class="color blue">Abigail Child</span>. Experimental film and video
      maker.
    end: '0:26:00'
    start: '0:25:00'
  - body: >-
      <span class="color blue">Truc Ha</span>. Project assistant on Women of
      Vision. Undergraduate student at Bryn Mawr College.
    end: '0:31:00'
    start: '0:30:40'
  - body: >-
      <span class="color blue">Patty White</span>. Scholar of feminist, lesbian,
      and queer cinema.
    end: '0:32:40'
    start: '0:32:00'
  - body: >-
      Task one: a timeline of feminist film and video history. When should it
      start: with who or what? <span class="color blue">Annie Sprinkle</span>
      starts strong: “let’s begin with the first naked images shot of women,”
      i.e. the beginning of cinema. <span class="color blue">Abigail
      Child</span> goes further back to pre-cinema.
    end: '0:36:30'
    start: '0:35:00'
  - body: >-
      <span class="color blue">Annie Sprinkle</span> responds and adds <span
      class="color blue">Maya Deren</span> and <span class="color blue">Germain
      Dulac</span>. <span class="color blue">Su Friedrich</span> adds <span
      class="color blue">Alice Guy Blache</span>. <span class="color blue">Getta
      Reddy</span> asks: “why are we only focusing on people for this history?"
      <span class="color red">
    end: '0:42:00'
    images:
      - image: /archive/media/alice.jpg
      - image: /archive/media/maya.jpg
      - image: /archive/media/gdl.jpg
    start: '0:39:30'
  - body: >-
      <span class="color blue">Alisa Lebow</span>: Is this a history of
      production or spectatorship? Influence, inspiration, anger? Is it of all
      women or political women? <span class="color blue">Alex Juhasz</span>
      notes that this is a circular sense of history. <span class="color
      red">These circles inside of circles feel overwrought to me now. </span>
      <span class="color blue">Abigail Child</span>: “there are so few women,
      let’s try not to forget any.” It is asked: “Are you a feminist because you
      are doing things men have done before and women never did?”
    end: '0:53:00'
    start: '0:46:00'
  - body: >-
      <span class="color blue">E. Ann Kaplan</span> enters the conversation:
      feminist film scholar. She introduces <span class="color blue">Yvonne
      Rainer</span>: avant-garde feminist lesbian filmmaker and dancer who
      discusses dance, the role of women, and performance art. <span
      class="color blue">Jane Weinstock</span>, feminist filmmaker is caught in
      the frame. <span class="color red">At that time, I would have been a bit
      gob-smacked by these heavy feminist hitters. </span>
    end: '01:12:20'
    start: '01:11:00'
  - body: >-
      <span class="color blue">Mona Jimenez</span> enters the conversation—media
      activist and curator—and introduces ideas about video and organizing.
      <span class="color blue">Beth Stryker</span> mentions Good Machine and
      distribution.
    end: '1:25:00'
    start: '01:19:30'
  - body: ' <span class="color yellow"> Distracted here by seeing my wedding ring and realizing that my short about what happened to it--"The Ring Cycle"--is eighteen years in the future! ... And <span class="color blue">Sarah (Schulman)</span> produced a film, "United In Anger" (2012) that used the work of all those camcorder activists to educate a new generation of activists … and make a home movie. Your footage has that feel for me, too. This issue of spectatorship has stayed so alive … twenty-five years later we still need the Bechdel Test and the <span class="color blue">Geena Davis</span> Institute’s “If she can see it she can be it” campaign. Through that lens, it’s hard not to feel like we’ve failed the generations who came after us. Or at least dramatically underestimated the scope of the struggle. Contributed by</span> <span class="color blue">Erin Cramer</span>'
    end: '00:03:15'
    images:
      - image: /archive/media/united-in-anger.jpg
      - image: /archive/media/erin.jpg
    start: '00:02:46'
  - body: >-
      <span class="color yellow"> Me, too (gobsmacked, see above)!  We were!
      <span class="color blue">Jane Weinstock's</span> "Sigmund Freud’s Dora"
      (1979)--made with a collective--was so influential to me. Funnily enough,
      years after this meeting we became friends and offer each other support in
      the challenging world of narrative feature filmmaking. <span class="color
      blue">Jane Weinstock </span> has written and directed two narrative
      features since then. Interesting who stayed doing experimental work and
      who moved toward more narrative storytelling. Contributed by <span
      class="color blue"> Erin Cramer</span>
    end: '01:11:30'
    images:
      - image: /archive/media/sig-f-dora.jpg
      - image: /archive/media/moment.jpg
    start: '01:11:15'
  - body: >-
      <span class="color yellow"> Really interesting for me how much I keep
      circling back to your idea of maybe this is a history of what didn’t get
      made. Realizing how key it remains for women in film—though, to be fair,
      film in general—and how, actually, that approach hasn’t ever been taken.
      Wow, and the funding question goes to the heart of it for me and makes
      watching this foreground the bitter in bittersweet for a sec! Contributed
      by <span class="color blue"> Erin Cramer</span>
    end: '00:55:35'
    start: '00:55:30'
  - body: >-
      <span class="color yellow"> This was during my first year or so as
      Director of Media Alliance, where organizing was happening around video
      preservation to save early video art and community video. Media Alliance
      was distributing "Video Preservation: Securing the Future of the Past" by
      <span class="color blue">Deirdre Boyle</span> that came out of a 1991
      symposium at the Museum of Modern Art organized by <span class="color
      blue">Debbie Silverfine</span> (New York State Council on the Arts) and
      <span class="color blue">Mary Esbjornson</span> (Media Alliance). My
      experience with video was both as an artist and as an organizer, so I
      always sought to bring those two worlds together. There has been an uptake
      in archival projects that represent debates/decision-making/creative
      projects in feminist and other social justice spaces, but I am not sure
      they are easy to find or use by educators and makers. The <a
      href="communityarchiving.org"> Community Archiving Workshop</a> model
      seeks to collectively process tapes held by a range of organizations to
      encourage their caretakers to undertake preservation projects. Contributed
      by <span class="color blue">Mona Jimenez.</span>
    end: '01:21:00'
    images:
      - image: /archive/media/videopreservatio00boyl_0001.jpg
      - image: /archive/media/subject-to-change.jpg
    start: '01:20:00'
  - body: >-
      <span class="color yellow"> WOW its so amazing to see myself. I can't
      believe how much has and has not changed about the progresses and failures
      and invisibility of women in the film arts. Contributed by <span
      class="color blue"> Cheryl Dunye</span>
    end: '00:21:00'
    start: '00:20:44'
  - body: >-
      <span class="color yellow"> <span class="color blue">Annie
      Sprinkle!</span> Talking about how she transitions to a changing audience
      in porn. Annie changed way more than that. She was creating the framework
      for feminist porn. Folks like <span class="color blue">Shine Lousie
      Houston</span> and <span class="color blue">Jiz Lee</span> continue this
      tradition  Contributed by <span class="color blue"> Cheryl Dunye</span>
    end: '00:25:00'
    images:
      - image: /archive/media/shine.jpg
      - image: /archive/media/jiz.jpg
    start: '00:24:48'
  - body: >-
      <span class="color yellow"> What??? Did that make any sense at all? Happy
      to see how these conversations throughout the years have helped strengthen
      my mode of production, my work, and more importantly my pontification
      skills ... HA! Contributed by <span class="color blue"> Cheryl
      Dunye</span>
    end: '02:59:00'
    start: '02:58:42'
  - {}
  - {}
  - {}
  - body: >-
      <span class="color yellow"> To what <span class="color blue">Abigail Child
      says</span>: "It \[of the 70s work] came out of a collective
      consciousness. And collective work. There was no film history." I would
      add: We had to work simultaneously on multiple levels: 1) create the new
      media 2) dig up the history 3) get the movies to audiences 4) establish
      the institutional structures that would sustain our work. It was a very
      large basket of responsibilities and work. Contributed by <span
      class="color blue"> Ariel Dougherty</span>
    end: '01:01:00'
    start: '01:00:00'
  - body: >-
      <span class="color yellow"> The comment above posted by <span class="color
      blue"> Erin Cramer</span>, “dramatically underestimated the scope of the
      struggle,” is instructive. Both <span class="color blue">Laura
      Mulvey</span> and I speaking the same day, but in different forums at the
      2016 Experiments in Cinema \[ABQ, NM] stated a variation on this. We
      phrased it more along the lines, that when we started out in the early
      1970s, we had expected by now that we would be MUCH further along.
      Contributed by <span class="color blue"> Ariel Dougherty</span> in a
      longer email (see below).
    end: '00:03:00'
    files:
      - file: /archive/media/ariel-dougherty-email-response.pdf
        filethumb: /archive/media/ad-email-pic.jpg
    images:
      - image: /archive/media/laura-m.jpg
    start: '00:02:50'
  - body: >-
      <span class="color blue"> Erin Cramer</span> (48:35) <span class="color
      yellow"> succinctly says: "Two things: the history of spectatorship and
      then the history of production...." Several people skirt around (48:53)
      “what a feminist politics of production might be...“ (<span class="color
      blue"> Terry Lawler</span> 55:01+) This is necessary and your profile
      approach in the completed videos and accompanying book miss this vision: a
      working apparatus. In 1975 with the On-Going or Womanifesto we started
      this. It was a counterpoint to Mulvey's gaze analysis (that still was not
      in our hands in February '75). <span class="color blue">B. Ruby
      Rich</span> in her 1978 article (first in "Jump Cut"; republished as
      Chapter 5 of "Chick Flicks") starts to articulate the differences. But she
      gives pages to what she calls the British/realistic theory (accredited to
      people with names); and just a paragraph or two to what she dubs the
      "American/optimistic" approach (no-names given). But I think some
      reexamination of this with present-day updates follows what <span
      class="color blue">Abigail Child</span> states (52:06) about “turning
      around Mulvey's theory and looking at how people on the margins are
      looking at cinema.” This might have a big impact on academia. Contributed
      by <span class="color blue"> Ariel Dougherty</span>
    end: '00:53:00'
    images:
      - image: /archive/media/ruby.jpg
    start: '00:52:06'
  - body: >-
      <span class="color blue"> Juanita Szczepanski </span> <span class="color
      yellow"> contributes these words after a <a href=” https://www.centerforthehumanities.org/programming/collective-visions-the-past-present-and-future-of-feminist-media “> 2020 public presentation</a>: As I listened to the women in this amazing tape, it occurred to me,  I realized I was one of them. It has become important to understand what make women go or not go to view a film. How are they motivated by issues of politics; technology; finances; diversity; education; distribution; knowledge; sexuality, immigration? In relation to film making does the scholar and less educated woman see the same films, does it mean the same to both? Should filmmakers know film history, if they are to make relevent material? In 2020 we are taking baby steps and gigantic leaps at the same time. Women’s film is just beginning to reflect women of color, seniors, disability, trans community, women in prisons, non-traditional jobs, etc.  Women from such diverse backgrounds cannot use a camera.  During thirty-some years working with young women, I have found them to be humble with a thirst to learn the hands on as well as the history of women filmmaking. To those I would say: “Never compare yourself or work. Never let an idea or project go by because you think you do not know enough. You know what you know. You all have a story to tell!”</span>
    end: ' 0:17:15'
    start: '00:17:00'
---
Filmmaker and scholar, <span class="color blue">Alexandra Juhasz</span>, holds a research meeting, the second of five, this one at Women Make Movies in NYC in 1994. Over its four hours, the meeting is attended by more than twenty-five local media feminists, many of them a who's who of media feminism (then and now). Taped in VHS, the ideas and conversation generated, recorded, and transcribed (see Resources, to the right) were used as research for her subsequent three-part documentary <a href="https://www.snagfilms.com/films/title/women_of_vision_18_histories_in_feminist_film_and_video"> Women of Vision: 18 Histories in Feminist Film and Video </a> and its associated <a href="https://www.upress.umn.edu/book-division/books/women-of-vision"> book of transcribed interviews</a>. All of its tapes (twenty hour-long interviews shot for the documentary as well as the research footage from meetings in Philadelphia, Los Angeles, Chicago, and San Francisco) were eventually archived at the <a href="https://www.cinema.ucla.edu/collections/outfest-ucla-legacy-project"> Outfest UCLA Legacy Project</a>. The NY tape was recently digitized for experimentation on this community-based archiving tool, Analog Archive. Here, the 4+ hour recording has been loosely annotated by Juhasz (and others, <span class="color yellow"> in yellow</span>) to highlight people <span class="color blue">(in blue)</span>, content (in white), and her own present-day observations or feelings raised by her twenty-five year later re-visit for the purpose of annotatation <span class="color red">(red, and in the I voice)</span>. While the tape sits alone here, it is also part of a larger revisit to 90s feminist activist media: a special issue of <a href="https://fmh.ucpress.edu/content/5/4"> Feminist Media Histories</a>, co-edited by Juhasz and Angela Aguayo, where more connections between feminists, histories, process, and activisms are carefully considered by a range of authors working cross-generationally within a variety of formats and methods.
