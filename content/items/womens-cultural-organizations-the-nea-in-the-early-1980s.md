---
title: Women's Cultural Organizations & the NEA in the early 1980s A
itemType: gallery
poster: /archive/media/dougherty-a.jpg
collections:
  - {}
resources:
  - {}
  - file: /archive/media/a.-women-arts-nea-early-1980s_.pdf
keywords:
  - {}
related:
  - items:
      - {}
images:
  - {}
  - image: /archive/media/dougherty-a.jpg
---
In the autumn of 1980, at the height of Ronald Reagan’s bid for the presidency, The Heritage Foundation issued a scathing attack---out of the blue---against both the National Endowment for the Arts and the National Endowment for the Humanities. The report condemned any form of culture that had a social context (forget the fact that it all does!). For Ariel Dougherty alarm bells went off. The NEA budget continued to rise in the early 1980s, but the numbers of women’s cultural organizations supported by the federal agency began to decline. A chilling effect had begun. Dougherty did five things. In TWO DOCS, A & B. Contributed as PDF by Ariel Dougherty.
