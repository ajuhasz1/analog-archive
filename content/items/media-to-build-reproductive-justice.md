---
title: Media to Build Reproductive Justice
itemType: gallery
poster: /archive/media/media-to-build-rj.jpg
collections:
  - {}
resources:
  - {}
  - file: /archive/media/rjmedia-resources_a.dougherty_may-2019-copy.pdf
keywords:
  - {}
related:
  - items:
      - {}
images:
  - {}
---
Media – film, video, DVD, (live-)streaming & art— is a great way to do community outreach

and education. This is a short list of media works that can assist in building community. A

few audio works and archives of special interest are included. Distributors have other

works, so check out their websites. And don’t forget to ask for special community rates.

Good speakers and discussion facilitators are helpful in engaging audience participation.

Organizing is an art!
