---
title: Snapshot of Foundation Support for Feminist Gender Justice Media
itemType: gallery
poster: /archive/media/snapshot.jpg
collections:
  - {}
resources:
  - {}
  - file: /archive/media/snapshot-statusofsupport-4gjmedia-2010-copy.pdf
keywords:
  - {}
related:
  - items:
      - {}
images:
  - {}
---
SNAPSHOT of FOUNDATION SUPPORT

for FEMINST GENDER JUSTICE MEDIA

by Ariel Dougherty

A report from Catalyst Fund of Tides Foundation that focuses on funding women of color-led

reproductive justice organizations inspired an examination into the actual support by foundations of feminist

gender justice media (FGJM) organizations. The 3-page chart that displays those foundations/funders and a

sample of 20 FGJM organizations that they fund is attached, starting on page 7.
