import React from 'react'
import { createPortal } from 'react-dom'

const overlayNode = document.getElementById('mini-player-root')

const FloatingMedia = (props) => {
  const { children } = props
  return createPortal(
    <div>
      {children}
    </div>,
    overlayNode
  )
}

export default FloatingMedia
