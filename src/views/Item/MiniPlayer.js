import React, { memo } from 'react'
import ReactPlayer from 'react-player'
import { withItem } from './ItemContext'

const MiniPlayer = memo((props) => {
  const { item, userPlaying, media, muted } = props
  const { player: { actions, ref }, poster } = item
  console.log(props)

  return (
    <ReactPlayer
      ref={ref}
      muted={muted}
      onDuration={actions.handleDuration}
      onEnded={actions.handleEnded}
      onProgress={actions.handleProgess}
      onStart={actions.handlePlaying}
      onPlay={actions.handlePlaying}
      onPause={actions.handlePause}
      url={media}
      playing={userPlaying}
      width='100%'
      height='100%'
      config={{ file: { attributes: { poster } } }}
    />
  )
})

MiniPlayer.defaultProps = {
  muted: false,
}

export default withItem(MiniPlayer)
