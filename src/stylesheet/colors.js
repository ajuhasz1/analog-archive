export default {
  black: '#000',
  white: 'rgb(220,220,220)',
  grey: 'rgb(150,150,150)',
  green: 'rgb(198,222,199)',
  lightBrown: '#89827A',
  brown: '#7D766F',
  darkBrown: '#4A4642',
}
